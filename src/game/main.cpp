
#include "StringUtils.h"
#include "SocketAddress.h"
#include "SocketAddressFactory.h"
#include "UDPSocket.h"
#include "TCPSocket.h"
#include "SocketUtil.h"

#if _WIN32
#include <Windows.h>
#endif
#include <iostream>

#if _WIN32
int WINAPI WinMain(_In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance, _In_ LPSTR lpCmdLine, _In_ int nCmdShow)
{

	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);
	SocketUtil::StaticInit();

	UDPSocketPtr sockptr = SocketUtil::CreateUDPSocket(SocketAddressFamily::INET);
	std::shared_ptr<SocketAddress> toAddress = SocketAddressFactory::CreateIPv4FromString("127.0.0.1:7");

	int result=sockptr->SendTo("Hello World", 12, *toAddress);


	std::cout << "Result: " << result << std::endl;

	SocketUtil::ReportError(std::to_string(result).c_str());

	SocketUtil::CleanUp();
	//Sockptr will go out of scope and delete here

	return result;
}

int main(void)
{
	SocketUtil::StaticInit();

	UDPSocketPtr sockptr = SocketUtil::CreateUDPSocket(SocketAddressFamily::INET);

	std::shared_ptr<SocketAddress> toAddress = SocketAddressFactory::CreateIPv4FromString("127.0.0.1:7");

	int result = sockptr->SendTo("Hello World", 12, *toAddress);

	std::cout << "Result: " << result << std::endl;

	char  buffer[32];

	result  = sockptr->ReceiveFrom(buffer, 32, *toAddress);

	std::cout << "Result2 : " << result << std::endl;

	std::cout << buffer << std::endl;


	SocketUtil::CleanUp();
	
	//Sockptr will go out of scope and delete here
	return result;
}

#else
const char** __argv;
int __argc;
int main(int argc, const char** argv)
{
	__argc = argc;
	__argv = argv;

}
#endif
